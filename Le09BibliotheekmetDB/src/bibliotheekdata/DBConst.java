package bibliotheekdata;

public class DBConst {
  public static final String DRIVERNAAM = "com.mysql.jdbc.Driver";
  public static final String URL = "jdbc:mysql://127.0.0.1/bibliotheek?useSSL=false&user=ou";
  public static final String GEBRUIKERSNAAM = "ou";
  public static final String WACHTWOORD = "";
}
